FROM openjdk:16-alpine

LABEL maintainer "thoratou"

## Dependencies ##

RUN apk add --no-cache -U \
  openssl \
  su-exec \
  bash \
  curl iputils wget \
  jq \
  screen \
  python3 python3-dev py3-pip \
  gcc \
  musl-dev \
  inotify-tools


## McStatus Health Check ##

RUN pip install mcstatus

RUN mkdir -p opt/mc-status-api
WORKDIR /opt/mc-status-api

HEALTHCHECK CMD mcstatus 127.0.0.1:25565 ping


## User / Group ##

RUN addgroup -g 1000 minecraft \
  && adduser -Ss /bin/false -u 1000 -G minecraft -h /home/minecraft minecraft \
  && mkdir -m 777 /data /mods /config /plugins \
  && chown minecraft:minecraft /data /config /mods /plugins /home/minecraft


## Use Minecraft User from this point ##

ENV UID=1000 GID=1000
USER minecraft
WORKDIR /home/minecraft

ENV PATH="/home/minecraft/.local/bin:$PATH"


## Envtpl ##
RUN pip install --user envtpl


## Minecraft config ##

COPY --chown=minecraft:minecraft bootstrap/server.properties.tpl /home/minecraft/server.properties.tpl
RUN touch /home/minecraft/white-list.txt

## Minecraft server daemon ##

COPY --chown=minecraft:minecraft init.d/minecraft /etc/init.d/minecraft
RUN chmod 755 /etc/init.d/minecraft
RUN mkdir -p /home/minecraft/server
RUN mkdir -p /home/minecraft/world

## Minecraft backup ##

RUN mkdir -p /home/minecraft/minecraft.backup

## Ports ##

EXPOSE 25565 27015


## Monitor script ##

COPY --chown=minecraft:minecraft monitor.sh /home/minecraft/monitor.sh
RUN mkdir -p /home/minecraft/serverstatus && echo "Stopped" > /home/minecraft/serverstatus/status

## Start ##

RUN echo 'eula=true' > /home/minecraft/eula.txt

CMD envtpl --keep-template /home/minecraft/server.properties.tpl && ./monitor.sh
