#!/bin/bash

cleanup()
{
  echo "Signal catched, stopping Minecraft container"
  echo "Minecraft container stopped"
  exit 0
}

statuswaitlog()
{
  while inotifywait -q -e modify /home/minecraft/serverstatus/status >/dev/null; do
    STATUS=$(cat /home/minecraft/serverstatus/status)
    echo "Minecraft server status changed: $STATUS"
  done
}

trap cleanup SIGKILL SIGTERM SIGHUP SIGINT EXIT;

echo "Minecraft container started"
statuswaitlog &
while :; do
    sleep 1s
done
